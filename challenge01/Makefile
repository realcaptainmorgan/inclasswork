# File Name: Makefile
# Author: Matthew Morrison
# E-mail: matt.morrison@nd.edu 
#
# This file contains the Makefile for compiling both possible 
# solutions, both struct and class, for Challenge 1.
#
# LIST OF MAKE COMMANDS AVAILABLE:
# - make C01STR 		- Compiles the version with a DATE Struct 
# - make C01CLA 		- Compiles the version with a DATE Class
# - make all    		- Compiles both versions simultaneously 
# - make cleanC01STR 	- Cleans the Struct version 
# - make cleanC01CLA 	- Cleans the Class version 
# - make cleanest    	- Cleans both versions 

# First, identify the compiler to be used.
C++ = g++

# Next, add the compiler flags, which we call CFLAGS
# -g    			adds debugging information to the executable file
# -std::gnu++11 	GNU C++ compiler, g++, provides extensions to the C++ language
#					For more info on gnu++11 https://gcc.gnu.org/onlinedocs/gcc/C_002b_002b-Extensions.html
# -Wpedantic		Issue all the warnings demanded by strict ISO C and ISO C++	
CFLAGS = -g -std=gnu++11 -Wpedantic

########################### Command: make C01Str ###############################

# First, create a message to send to the user
strMsg = $(info ********COMPILING STRUCT VERSION************)

# Next, build the build target executable:
DATESTR = testDateStr

# Next, indicate the compilation commands
# Put together, this becomes: g++ -g -std=gnu++11 -Wpedantic testDateStr.cpp -o testDateStr
strCmp = $(C++) $(CFLAGS) $(DATESTR).cpp -o $(DATESTR)

# Command: make C01STR
# output: ********COMPILING STRUCT VERSION************
# output: g++ -g -std=gnu++11 -Wpedantic testDateStr.cpp -o testDateStr
C01STR: 
	$(strMsg) 
	$(strCmp)

######################### Command: make C01CLA ###############################

# First, create a message to send to the user
claMsg = $(info ********COMPILING CLASS VERSION************)

# Next, we will do everything for the Data Class case.
DATECLA = testDateCla
# Put both cpp files into claCPP, since the .h files connect them.
claCPP = $(DATECLA).cpp dateCla.cpp

# Next, indicate the compilation commands
# Put together, this becomes: g++ -g -std=gnu++11 -Wpedantic testDateCla.cpp dateCla.cpp -o testDateCla
claCmp =  $(C++) $(CFLAGS) $(claCPP) -o $(DATECLA)

# Command: make C01CLA
# output: ********COMPILING CLASS VERSION************
# output: g++ -g -std=gnu++11 -Wpedantic testDateCla.cpp dateCla.cpp -o testDateCla
C01CLA: 
	$(claMsg) 
	$(claCmp)			

######################### Command: make all ###############################

# First, create a message to the user that this is for both versions
bothMsg = $(info ********COMPILING BOTH VERSIONS WITH make all************)

# Command: make all
# Output: ********COMPILING BOTH VERSIONS WITH make all************
# Output: g++ -g -std=gnu++11 -Wpedantic testDateStr.cpp -o testDateStr
# Output: g++ -g -std=gnu++11 -Wpedantic testDateCla.cpp dateCla.cpp -o testDateCla
all: 
	$(bothMsg) 
	$(strCmp) 			
	$(claCmp)			

######################### Command: make cleanC01Str ###############################

cleanStrMsg = $(info ********CLEAN STRUCT VERSION************)
cleanStr = $(RM) $(DATESTR) *.o *.out

# Command: make cleanC01Str
# Output: 	********CLEAN STRUCT VERSION************
# Output: 	rm -f testDateStr *.o *.out
cleanC01STR: 
	$(cleanStrMsg)
	$(cleanStr)


######################### Command: make cleanC01Cla ###############################

cleanClaMsg = $(info ********CLEAN CLASS VERSION************)
cleanCla = $(RM) $(DATECLA) *.o *.out

# Command: make cleanC01Cla
# Output:	********CLEAN CLASS VERSION************
# Output: 	rm -f testDateCla *.o *.out
cleanC01CLA: 
	$(cleanClaMsg)
	$(cleanCla)


######################### Command: make cleanest ###############################
cleanestMsg = $(info ********CLEANEST VERSION************)

# Command: make cleanest
# Output: ********CLEANEST VERSION************
# Output: rm -f testDateStr *.o *.out
# Output: rm -f testDateCla *.o *.out
cleanest: 
	$(cleanestMsg)
	$(cleanStr)
	$(cleanCla)